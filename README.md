# **Create Clone Project**


1. Create a clone from this url 'git clone https://gitlab.com/Ihjazfathima/webpage_nextjs.git '
2. You will get a folder named "signin" .
3. Open signin folder and select the path & enter the "cmd" comment in folder path then you will get comment  prompt and Enter "code ."
4. Automatically that application will open in visual studio code.


# **Working Process**

1. Run the application (npm run dev) and select the application url http://localhost:3000
2. The chrom will open with title "Sign In" and fields like "Enter username","Enter email","Enter password".
3. After entering the details by clicking "sign up" button you will redirect to page http://localhost:3000/dashboard with the command "Welcome to Dashboard !".
4. By clicking "Back to Login page button" you can go back to "Sign In" page.

      
